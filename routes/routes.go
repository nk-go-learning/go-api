package routes

// Table containing all routes
type Table struct{}

// CreateRoutes returns a route table
func CreateRoutes() *Table {
	return &Table{}
}
