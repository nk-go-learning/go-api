package routes

import (
	"lux-api/models"
	"testing"
)

func assertNoError(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatalf("didn't expect an error but got one, %v", err)
	}
}

func assertUser(t *testing.T, got, want models.User) {
	t.Helper()

	if got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}
