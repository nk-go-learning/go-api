package routes

import (
	"encoding/json"
	"lux-api/models"
	"lux-api/server"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLogin(t *testing.T) {
	routes := CreateRoutes()
	server := server.NewLuxServer(routes)

	t.Run("can login with valid account", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/login", nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		var got models.User
		err := json.NewDecoder(response.Body).Decode(&got)
		assertNoError(t, err)

		want := models.User{
			Name: "nick",
		}

		assertUser(t, got, want)
	})
}
