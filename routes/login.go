package routes

import (
	"encoding/json"
	"lux-api/db"
	"net/http"
)

// Login route
func (t *Table) Login(w http.ResponseWriter, r *http.Request) {

	db := db.GetDb()
	result, err := db.Login("nick", "")

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(result)
}
