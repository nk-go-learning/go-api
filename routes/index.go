package routes

import (
	"fmt"
	"net/http"
)

// Home render the home route
func (t *Table) Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Index page")
}
