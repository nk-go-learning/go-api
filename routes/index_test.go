package routes

import (
	"lux-api/server"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndexRenders(t *testing.T) {

	routes := CreateRoutes()
	server := server.NewLuxServer(routes)

	t.Run("renders index page", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/", nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		got := response.Body.String()
		want := "Index page"

		if got != want {
			t.Errorf("got %s, want %s", got, want)
		}
	})
}
