package db

import (
	"errors"
	"fmt"
	"lux-api/models"
)

// Db struct
type Db struct {
	users map[string]models.User
}

var db *Db

// Login performs a login with the given user and password
func (d *Db) Login(username, password string) (*models.User, error) {

	loggedInUser, ok := db.users[username]

	if !ok {
		return nil, errors.New("user not found")
	}

	// TODO: check password...

	return &loggedInUser, nil
}

func init() {
	db = &Db{
		users: map[string]models.User{
			"nick": models.User{
				Name: "nick",
			},
		},
	}
	fmt.Println("init called in db")
}

// GetDb returns the db instance
func GetDb() *Db {
	return db
}
