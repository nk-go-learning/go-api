package server

import (
	"net/http"
)

// Routes interface
type Routes interface {
	Home(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
}

// LuxServer struct
type LuxServer struct {
	http.Handler
	Routes Routes
}

// NewLuxServer creates a new LuxServer to be used
func NewLuxServer(routes Routes) *LuxServer {
	l := &LuxServer{
		http.NewServeMux(),
		routes,
	}

	// TODO: register routes
	router := http.NewServeMux()
	router.Handle("/", http.HandlerFunc(routes.Home))
	router.Handle("/login", http.HandlerFunc(routes.Login))

	l.Handler = router

	return l
}
