package main

import (
	"log"
	"lux-api/routes"
	"lux-api/server"
	"net/http"
)

func main() {
	routes := routes.CreateRoutes()
	server := server.NewLuxServer(routes)

	if err := http.ListenAndServe(":9999", server); err != nil {
		log.Fatalf("could not listen on port 9999 %v", err)
	}
}
